'use strict';

const serverStatic = require('serve-static');
const modRewrite = require('connect-modrewrite');

const socket = require('./lib/socket');

module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    connect: {
      options: {
        port: 9000,
        // Change this to '0.0.0.0' to access the server from outside.
        hostname: '0.0.0.0',
        livereload: process.env.LIVE_RELOAD_PORT || 35729
      },
      livereload: {
        options: {
          open: true,
          onCreateServer: socket,
          middleware: function(connect) {
            return [
              modRewrite(['^[^\\.]*$ /index.html [L]']),
              connect().use('/templates', serverStatic('./dist')),
              serverStatic('demo-app/dist')
            ];
          }
        }
      }
    },
    watch: {
      demoapp: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          './demo-app/src/**/*.ts',
          './demo-app/src/**/*.html',
          './demo-app/src/**/*.css'
        ],
        tasks: ['exec:demo-build']
      },
      templates: {
        options: {
          livereload: '<%= connect.options.livereload %>'
        },
        files: [
          './src/{,*/}*.html',
          './src/{,*/}*.scss',
          './src/*/mocks/**/*.{png,jpg,jpeg,gif,webp,svg,json}',
          './Gruntfile.js'
        ],
        tasks: ['build:dev']
      }
    },
    exec: {
      'demo-deps': {
        sync: false,
        cwd: 'demo-app',
        cmd: 'npm install'
      },
      'demo-build': {
        sync: false,
        cwd: 'demo-app',
        cmd: 'ng build'
      }
    },
    sass: {
      dist: {
        options: {
          sourcemap: 'none'
        },
        files: [{
          expand: true,
          cwd: 'src',
          src: ['**/*.scss'],
          dest: 'dist',
          ext: '.css'
        }]
      }
    },
    clean: {
      dist: ['dist']
    },
    copy: {
      dist: {
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['**/*', '!**/*.scss', '!**/mocks/**', '!**/*.DS_Store', '!**/*Thumbs.db'],
            dest: 'dist'
          }
        ]
      },
      dev: {
        files: [
          {
            expand: true,
            cwd: 'src/',
            src: ['**/*', '!**/*.scss', '!**/*.DS_Store', '!**/*Thumbs.db'],
            dest: 'dist'
          }
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-exec');

  grunt.registerTask('build:dist', ['clean', 'sass:dist', 'copy:dist']);
  grunt.registerTask('build:dev', ['clean', 'sass:dist', 'copy:dev']);

  grunt.registerTask('default', ['build:dist']);

  grunt.registerTask('serve', [
    'build:dev',
    'exec:demo-deps',
    'exec:demo-build',
    'connect:livereload',
    'watch'
  ]);
};
