import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import {NgxDynamicTemplateModule} from 'ngx-dynamic-template';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { TemplateViewerComponent } from './template-viewer/template-viewer.component';


@NgModule({
  declarations: [
    AppComponent,
    TemplateViewerComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    NgxDynamicTemplateModule.forRoot({}),
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
