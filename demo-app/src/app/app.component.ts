import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {WebcastudioPlayerModule} from '@webcastudio/player';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  public manifest: any;
  public modules = [WebcastudioPlayerModule];
  public context: any = {};
  public template = '';

  constructor(private http: HttpClient) {

  }
  ngOnInit() {
    this.http.get('/templates/complete/manifest.json')
    .subscribe(manifest => {
      this.manifest = manifest;

      this.manifest.event = {
        name: 'Event 1',
        id: 1,
        template: {
          styles: `/templates/complete/styles.css`
        }
      };
      this.manifest.language = {
        id: 1,
        code: 'en'
      };
      this.manifest.session = {
        id: 1,
        name: 'Session 1'
      };

      this.context.manifest = this.manifest;

      this.http.get('/templates/complete/page.html', {
        responseType: 'text'
      })
      .subscribe(html => {
        this.template = `<ws-webcast-container [manifest]="manifest">${html}</ws-webcast-container>`;
      })

    });
  }
}
