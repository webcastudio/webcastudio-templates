import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TemplateViewerComponent} from './template-viewer/template-viewer.component';

const routes: Routes = [{
  path: ':template',
  component: TemplateViewerComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
