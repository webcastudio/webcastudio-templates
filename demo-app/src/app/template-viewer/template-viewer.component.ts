import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {HttpClient} from '@angular/common/http';
import {switchMap} from 'rxjs/operators';
import {WebcastudioPlayerModule} from '@webcastudio/player';

@Component({
  selector: 'app-template-viewer',
  templateUrl: './template-viewer.component.html',
  styleUrls: ['./template-viewer.component.css']
})
export class TemplateViewerComponent implements OnInit {

  public templateName: string;
  private manifestUrl: string;
  private templateUrl: string;

  public modules = [WebcastudioPlayerModule];
  public context: any = {};
  public template = '';

  constructor(private route: ActivatedRoute, private http: HttpClient) { }

  ngOnInit() {
    this.templateName = this.route.snapshot.params['template'];
    this.manifestUrl = `/templates/${this.templateName}/manifest.json`;
    this.templateUrl = `/templates/${this.templateName}/page.html`;

    this.http
      .get(this.manifestUrl)
      .pipe(
        switchMap(() => this.http.get(this.templateUrl, {responseType: 'text'}),
          (manifest, template) => ({
            manifest,
            template
          })
        )
      )
      .subscribe(result => {
        const manifest: any = Object.assign({}, result.manifest);

        manifest.event = {
          name: 'Event 1',
          id: 1,
          template: {
            styles: `/templates/${this.templateName}/styles.css`,
            manifest: result.manifest
          }
        };
        manifest.language = {
          id: 1,
          code: 'en'
        };
        manifest.session = {
          id: 1,
          name: 'Session 1'
        };
        manifest.interaction = {
          socket: {
            enabled: true,
            url: ''
          }
        };
        manifest.authorization = 'link:token';
        manifest.streamOdService = {
          interval: 0,
          url: `/templates/${this.templateName}/mocks/ondemand.json`
        };
        manifest.contentsService = {
          interval: 0,
          url: `/templates/${this.templateName}/mocks/contents.json`
        };

        this.context.manifest = manifest;
        this.template = `<ws-webcast-container [manifest]="manifest">${result.template}</ws-webcast-container>`;

      });
  }

}
