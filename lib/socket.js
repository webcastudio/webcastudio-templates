let ids = 0;
const lorem = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. ' +
  'Phasellus nec tristique erat. Nulla dictum porta feugiat. Duis ' +
  'consectetur libero tincidunt mi lobortis, sit amet accumsan sem ' +
  'condimentum. Duis eget tincidunt nibh. Cras elementum ligula eu ' +
  'condimentum fringilla. Aenean ut consequat sem. Vivamus venenatis diam ' +
  'non dolor tempor consectetur. Proin at risus ac nisl ornare tincidunt. ' +
  'Integer ac tortor dui. Aliquam erat volutpat. Pellentesque eu elementum ' +
  'odio.';

const users = [{
  id: 1,
  email: 'john@acme.com',
  firstName: 'John',
  lastName: 'Smith'
}];

function rand(len) {
  return Math.floor((Math.random() * len));
}
function generateQuestions(len) {
  const result = [];

  for (let i = 1; i <= len; i++) {
    const start = rand(lorem.length / 2);
    const end = rand(lorem.length - start) + 10;

    result.push({
      id: ids++,
      endUserId: 1,
      body: lorem.substr(start, end),
      sessionId: 1,
      answers: [],
      tags: 0,
      createdAt: new Date().toISOString(),
      updatedAt: new Date().toISOString(),
      endUser: users[rand(users.length)]
    })
  }
  return result;
}

module.exports = function(server) {
  const io = require('socket.io').listen(server);

  const questions = generateQuestions(rand(200 - 10));

  io
    .of('/qa')
    .on('connection', socket => {
      socket.on('getQuestions', ack => {
        ack(null, questions);
      });
    });
};
